FROM ruby:2.7.5

RUN apt-get update -qq && apt-get install -y postgresql-client

ADD . /myapp
WORKDIR /myapp
RUN bundle install

EXPOSE 3000

# Configure the main process to run when running the image
CMD ["bash"]