# README

* Ruby version: 2.7.5
* System dependencies
	* docker

## Configuration

```
$ docker compose up
```

Access localhost:3000

## Database creation

After `docker compose up`

```
$ docker-compose run web rake db:create db:migrate
```

## How to run the test suite

```
$ docker-compose run web rspec spec --format documentation
```

## API Feature

### Create Score: `POST /scores` with parameters:

*  `player`: a string containing the name of player. Case insensitive. EX: `大谷翔平`
*  `score`: a integer. EX: `98`
*  `time`: a atring representing date and time. EX: `2021-12-01 10:00:03 JST`
* returns a score object with json format containing `id`
	

### Get Score: `GET /scores/:id` 

It would return `404` if there is no such record.

EX: `GET /scores/3`


### Delete Score: `DELETE /scores/:id`

It would return `404` if there is no such record.

EX: `DELETE /scores/3`


### Get list of scores: `GET /scores` with optional parameters:

* `players`: a string containing multiple player names. EX: `Manic Chuang,兎田ぺこら`
*  `before`: a string representing date and time. EX: `2021-12-01 10:00:03 JST`
*  `after`: a string representing date and time. EX: `2021-12-01 10:00:03 JST`

### Get players' history: `GET /players/:name`

* `name` should be encoded if it contains non-ascii code. EX: `/players/Manic%20Chuang`
* returns a json format object contains related information

```
{
  "name": "大空スバル",
  "avg_score": 38,
  "top_score_record": {
    "id": 2,
    "score": 38,
    "time": "2021-12-02 19:30:00 +0900"
  },
  "low_score_record": {
    "id": 2,
    "score": 38,
    "time": "2021-12-02 19:30:00 +0900"
  },
  "scores": [
    {
      "id": 2,
      "score": 38,
      "time": "2021-12-02 19:30:00 +0900"
    }
  ]
}
```
