# frozen_string_literal: true

class PlayersController < ApplicationController
  def show
    render json: ScoreService.history(params[:id])
  end
end
