# frozen_string_literal: true

class ScoresController < ApplicationController
  def index
    scores = ScoreService.fetch(index_params)
    render json: {
      scores: scores.map(&:as_api_json),
      pagination: pagination(scores)
    }
  end

  def create
    score = ScoreService.create(create_params[:player], create_params[:score], create_params[:time])
    render json: score.as_api_json, status: :created
  end

  def show
    score = Score.find params[:id]
    render json: score.as_api_json
  end

  def destroy
    score = Score.find params[:id]
    score.destroy

    render json: { status: :ok }
  end

  protected

  def index_params
    permit = params.permit(:players, :before, :after, :page)
    permit.slice(:players, :before, :after).merge(
      page: [permit[:page].to_i, 1].max
    ).to_h.symbolize_keys
  end

  def create_params
    params.permit(:player, :score, :time)
  end
end
