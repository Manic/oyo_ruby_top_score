# frozen_string_literal: true

class Score < ApplicationRecord
  belongs_to :player

  def as_api_json(include_player: true)
    player = include_player ? { player: self.player.name } : {}
    {
      id: id,
      score: score,
      time: time.to_s
    }.merge(player)
  end
end
