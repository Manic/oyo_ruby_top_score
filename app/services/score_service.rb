# frozen_string_literal: true

module ScoreService
  def create(player_name, score, time)
    player = Player.create_or_find_by(
      name: normalize_player(player_name)
    )
    Score.create(
      player: player,
      score: score.to_i,
      time: Time.zone.parse(time)
    )
  end

  def fetch(players: nil, before: nil, after: nil, page: 1)
    players = players.to_s.split(',').map { |name| normalize_player(name) }
    after = Time.zone.parse(after.to_s)
    before = Time.zone.parse(before.to_s)
    records = Score.order(time: :desc)
    records = records.joins(:player).where(player: { name: players }) if players.present?
    records = records.where('time >= ?', after) if after.present?
    records = records.where('time <= ?', before) if before.present?
    records.page(page)
  end

  def history(player_name)
    player = Player.create_or_find_by(
      name: normalize_player(player_name)
    )
    score_records = player.scores.order(time: :desc)
    if score_records.empty?
      return {
        name: player.name,
        avg_score: 0,
        top_score_record: nil,
        low_score_record: nil,
        scores: []
      }
    end
    sorted_score_records = score_records.sort_by(&:score)

    top_score_record = sorted_score_records[-1]
    low_score_record = sorted_score_records[0]

    scores = score_records.map(&:score)
    avg_score = (scores.sum.to_f / scores.size).round
    {
      name: player.name,
      avg_score: avg_score,
      top_score_record: top_score_record.as_api_json(include_player: false),
      low_score_record: low_score_record.as_api_json(include_player: false),
      scores: score_records.map { |row| row.as_api_json(include_player: false) }
    }
  end

  def normalize_player(player_name)
    player_name.to_s.strip.split.map(&:camelcase).join(' ')
  end

  module_function :create, :normalize_player, :fetch, :history
end
