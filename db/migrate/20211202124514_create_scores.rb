# frozen_string_literal: true

class CreateScores < ActiveRecord::Migration[6.1]
  def change
    create_table :scores do |t|
      t.belongs_to :player, foreign_key: true
      t.integer :score
      t.datetime :time
      t.timestamps

      t.index %i[time player_id]
    end
  end
end
