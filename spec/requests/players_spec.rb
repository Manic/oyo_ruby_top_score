# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Players', type: :request do
  let(:player) { '大空スバル' }
  let(:score) { 97 }
  let(:time) { '2021-10-02 09:17' }

  describe '#show' do
    it 'trigger ScoreService.history' do
      ScoreService.create('大空スバル', 38, '2021-12-02 19:30')

      # Since we tested ScoreService in app/services, so
      # we don't have to test full features here.
      allow(ScoreService).to receive(:history).and_call_original

      get "/players/#{CGI.escape('大空スバル')}"

      expect(ScoreService).to have_received(:history).with('大空スバル')

      ret_json = JSON.parse(response.body)
      expect(ret_json).to include(
        'name' => '大空スバル',
        'avg_score' => 38
      ).and include('top_score_record', 'low_score_record', 'scores')
    end
  end
end
