# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Scores', type: :request do
  let(:player) { 'manic chuang' }
  let(:score) { 30 }
  let(:time) { '2021-10-02 09:17' }

  describe '#create' do
    let(:input) { { player: player, score: score, time: time } }

    it 'create players and scores' do
      post '/scores', params: input

      expect(response).to have_http_status(:created)
      expect(response.content_type).to include('application/json')
      expect(JSON.parse(response.body)).to include(
        'player' => 'Manic Chuang',
        'score' => score,
        'time' => Time.zone.parse(time).to_s
      ).
        and include('id')
    end

    it 'create players and scores (json payload)' do
      headers = { "CONTENT_TYPE" => "application/json" }

      post '/scores', params: input.to_json, headers: headers

      expect(response.content_type).to include('application/json')
      expect(JSON.parse(response.body)).to include(
        'player' => 'Manic Chuang',
        'score' => score,
        'time' => Time.zone.parse(time).to_s
      )
    end
  end

  describe '#show' do
    it 'return 404 if not found' do
      get '/scores/not_found'

      expect(response).to have_http_status(:not_found)
    end

    it 'return score json when found' do
      record = ScoreService.create(player, score, time)
      get "/scores/#{record.id}"

      expect(response).to have_http_status(:ok)
      expect(response.content_type).to include('application/json')
      expect(JSON.parse(response.body)).to include(
        'player' => 'Manic Chuang',
        'score' => score,
        'time' => Time.zone.parse(time).to_s
      )
    end
  end

  describe '#destroy' do
    it 'return 404 if not found' do
      delete '/scores/not_found'
      expect(response).to have_http_status(:not_found)
    end

    it 'render status ok when score found' do
      record = ScoreService.create(player, score, time)
      expect {
        delete "/scores/#{record.id}"
      }.to change { Score.count }.by(-1)
      expect(response).to have_http_status(:ok)
    end
  end

  describe '#index' do
    it 'returns data we want' do
      ScoreService.create('兎田ぺこら', 76, '2021-10-01 17:43:22')
      # Since we tested ScoreService in app/services, so
      # we don't have to test full features here.
      allow(ScoreService).to receive(:fetch).and_call_original

      get '/scores', params: { players: '兎田ぺこら,Manic chuang', before: '2021-11-06', after: '2021-01-01' }
      expect(ScoreService).to have_received(:fetch).with(
        players: '兎田ぺこら,Manic chuang',
        before: '2021-11-06',
        after: '2021-01-01',
        page: 1
      )
      ret_json = JSON.parse(response.body)
      expect(ret_json['pagination']).to include(
        'total_count' => 1,
        'limit_value' => 25,
        'total_pages' => 1,
        'current_page' => 1
      )
      expect(ret_json['scores'][0]).to include(
        'player' => '兎田ぺこら',
        'score' => 76
      )
    end
  end
end
