# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ScoreService do
  describe '.create(player_name, score, time)' do
    it 'create players and scores' do
      expect {
        ScoreService.create('Manic Chuang', 80, '2021-10-01 12:00')
        ScoreService.create('Manic chuang', 30, '2021-10-02 9:00')
      }.
        to change { Player.count }.by(1).
        and change { Score.count }.by(2)
    end
  end

  describe '.normalize_player(player_name)' do
    it 'trip and camelcase and remove redundent space' do
      expect(ScoreService.normalize_player('manic   chuang')).to eq('Manic Chuang')
      expect(ScoreService.normalize_player(' Manic chuang ')).to eq('Manic Chuang')
      expect(ScoreService.normalize_player(' 大谷翔平')).to eq('大谷翔平')
    end
  end

  describe '.fetch(players:, before:, after:, page:)' do
    it 'retrieve records with default limit_value 25' do
      # These tests could be a heavy SQL usage so I put these tests together.
      input = [
        ['Manic chuang', 90, '2021-12-02 23:24:24'],
        ['Manic chuang', 58, '2021-12-01 23:24:24'],
        ['Manic chuang', 99, '2021-11-30 23:24:24'],
        ['Manic chuang', 53, '2021-11-29 23:24:24'],
        ['Manic chuang', 97, '2021-11-28 23:24:24'],
        ['Manic chuang', 96, '2021-11-27 23:24:24'],
        ['Manic chuang', 51, '2021-11-26 23:24:24'],
        ['Manic chuang', 84, '2021-11-25 23:24:24'],
        ['Manic chuang', 71, '2021-11-24 23:24:24'],
        ['Manic chuang', 81, '2021-11-23 23:24:24'],
        ['Manic chuang', 92, '2021-11-22 23:24:24'],
        ['Manic chuang', 60, '2021-11-21 23:24:24'],
        ['Manic chuang', 72, '2021-11-20 23:24:24'],
        ['Manic chuang', 88, '2021-11-19 23:24:24'],
        ['Manic chuang', 96, '2021-11-18 23:24:24'],
        ['Manic chuang', 80, '2021-11-17 23:24:24'],
        ['Manic chuang', 90, '2021-11-16 23:24:24'],
        ['Manic chuang', 59, '2021-11-15 23:24:24'],
        ['Manic chuang', 84, '2021-11-14 23:24:24'],
        ['Manic chuang', 64, '2021-11-13 23:24:24'],
        ['Manic chuang', 50, '2021-11-12 23:24:24'],
        ['Manic chuang', 73, '2021-11-11 23:24:24'],
        ['Manic chuang', 50, '2021-11-10 23:24:24'],
        ['Manic chuang', 86, '2021-11-09 23:24:24'],
        ['Manic chuang', 85, '2021-11-08 23:24:24'],
        ['Manic chuang', 55, '2021-11-07 23:24:24'],
        ['Manic chuang', 94, '2021-11-06 23:24:24'],
        ['Manic chuang', 99, '2021-11-05 23:24:24'],
        ['Manic chuang', 81, '2021-11-04 23:24:24'],
        ['Manic chuang', 91, '2021-11-03 23:24:24'],
        ['兔田ぺこら', 88, '2021-12-01 17:43:22'],
        ['さくらみこ', 76, '2021-10-01 17:43:22']
      ]
      records = input.map { |r| ScoreService.create(*r) }

      scores = ScoreService.fetch
      # order by time desc
      expect(scores[0]).to eq(records[0])
      expect(scores[1]).to eq(records[1])
      expect(scores[2]).to eq(records[-2])

      scores = ScoreService.fetch(page: 2)
      # get page 2 value
      expect(scores[-1]).to eq(records[-1])

      scores = ScoreService.fetch(players: '兔田ぺこら,さくらみこ')
      # 2 records found
      expect(scores.size).to eq(2)

      scores = ScoreService.fetch(before: '2021-10-03')
      # 1 record found
      expect(scores.size).to eq(1)
      expect(scores[0]).to eq(records[-1])

      scores = ScoreService.fetch(after: '2021-12-01')
      # 1 record found
      expect(scores.size).to eq(3)
      expect(scores[0]).to eq(records[0])
      expect(scores[1]).to eq(records[1])
      expect(scores[2]).to eq(records[-2])

      scores = ScoreService.fetch(
        players: 'manic chuang', before: '2021-12-01',
        after: '2021-11-09'
      )
      # 22 records found
      expect(scores.size).to eq(22)
      expect(scores[0]).to eq(records[2])
      expect(scores[1]).to eq(records[3])
      expect(scores[2]).to eq(records[4])
    end
  end

  describe '.history(player_name)' do
    it 'when player has no score records' do
      ret = ScoreService.history('Gawr Gura')
      expect(ret).to include(
        name: 'Gawr Gura',
        avg_score: 0,
        top_score_record: nil,
        low_score_record: nil,
        scores: []
      )
    end

    it 'when player has score records' do
      input = [
        ['戌神ころね', 90, '2021-12-02 23:24:24'],
        ['戌神ころね', 99, '2021-11-30 23:24:24'],
        ['戌神ころね', 58, '2021-12-01 23:24:24'],
      ]
      records = input.map { |r| ScoreService.create(*r) }

      ret = ScoreService.history('戌神ころね')
      expect(ret).to include(
        name: '戌神ころね',
        avg_score: 82,
        top_score_record: records[1].as_api_json(include_player: false),
        low_score_record: records[2].as_api_json(include_player: false),
      )
      expect(ret[:scores].size).to eq(3)
      expect(ret[:scores][0]).to eq(records[0].as_api_json(include_player: false))
      expect(ret[:scores][1]).to eq(records[2].as_api_json(include_player: false))
      expect(ret[:scores][2]).to eq(records[1].as_api_json(include_player: false))
    end
  end
end
